#ifndef _BTREE_H_
#include "element.h"
void btCreate(const std::string &fileName,int type, int length=-1);
int btFind(const std::string &fileName,const element &key);
void btInsert(const std::string &filename,const element &key,int value);
bool btDelete(const std::string &fileName, const element &key);
void seetree(const std::string &filename);
bool btExist(const std::string &fileName, const element &key);
std::set <int> btFindLess(const std::string &fileName, const element &key);
std::set <int> btFindMore(const std::string &fileName, const element &key);
#define _BTREE_H_
#endif
