GXX=g++ 
OPTM=-O2 -msse4 
CPFLAGS=$(OPTM) -Wall #-Werror
LDFLAGS=$(OPTM) -Wall

INCLUDES=-I. 
BINARY=minisql

OBJS=btree.o buffermanager.o catalogmanager.o element.o fitter.o interface.o interpreter.o main.o recordmanager.o table.o

all $(BINARY): $(OBJS)
	$(GXX) $(LDFLAGS) $(LIBS) $(OBJS) -o $(BINARY)

%.o: %.cc
	$(GXX) $(CPFLAGS) $(INCLUDES) -c $^ -o $@

%.o: %.cpp
	$(GXX) $(CPFLAGS) $(INCLUDES) -c $^ -o $@

%.o: %.c
	$(GXX) $(CPFLAGS) $(INCLUDES) -c $^ -o $@

clean:
	rm -rf $(OBJS)
	rm -rf $(BINARY)
