#ifndef _FITTER_H_
#define _FITTER_H_
#include "element.h"
#include <vector>
class Rule {
public:	
	int index;
	int type; // 0: < 1: <= 2: = 3: >= 4: > 5: <>
	element rhs;
	Rule(int index,int type,element rhs);
};
class Fitter {
public:
	std::vector <Rule> rules;
	Fitter();
	void addRule(const Rule &rule);
	bool test(const std::vector <element> &data) const;
};
#endif
