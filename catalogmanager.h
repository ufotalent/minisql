#ifndef _CATALOG_MANAGER_H_
#define _CATALOG_MANAGER_H_

#include <string>
#include <algorithm>
#include "table.h"
bool cmExistTable(const std::string &name);
table cmCreateTable(const std::string &name,const std::vector <item> &data);
table cmReadTable(const std::string &name);
void cmDropTable(const std::string &name);
bool cmExistIndex(const std::string &indexname);
bool cmRegisterIndex(const std::string &tablename,const std::string &indexname, int itemIndex);
std::pair<std::string,int> cmAskIndex(const std::string indexName);
bool cmDeleteIndex(const std::string indexName);
#endif
