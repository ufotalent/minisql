#ifndef _BUFFER_MANAGER_H_
#define _BUFFER_MANAGER_H_
#include <string>
const static int BlockSize=100;
const static int BufferSize=100;

struct Block {
	unsigned char data[BlockSize];
	unsigned int offset;
	std::string fileName;
};
Block bmNewBlock(const std::string &fileName);
Block bmReadBlock(const std::string &fileName, int offset);
void  bmWriteBlock(struct Block &b);
void  flushBuffer();
void  bmReleaseBlock(const std::string &fileName, int offset);
void  seebuffer();
void  bmClear(const std::string &fileName);
#endif
