#ifndef _INTERFACE_H_
#define _INTERFACE_H_
#include <string>
#include <vector>
#include "element.h"
#include "table.h"
#include "fitter.h"

struct Response {
	bool succeed;
	std::string info;
	std::vector <std::vector <element> > result;
	inline Response() { succeed=true;}
	inline Response(std::string data) { succeed=false; info=data;}	
	inline Response(std::vector <std::vector <element> > result):result(result),succeed(true){}
};
Response CreateIndex(const std::string indexName,const std::string &tableName , const std::string &itemName);
Response DropIndex(const std::string indexName);
Response CreateTable(const std::string &tableName,std::vector <item> data,int pk);
Response DropTable(const std::string &tableName);
Response Select(const std::string &tableName, const Fitter &fitter);
Response Delete(const std::string &tableName, const Fitter &fitter);
Response Insert(const std::string tableName, const std::vector<element> entry);
#endif
