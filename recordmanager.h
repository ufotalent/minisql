#ifndef _RECORD_MANAGER_H_
#define _RECORD_MANAGER_H_

#include <string>
#include <vector>
#include "table.h"
#include "element.h"
#include "fitter.h"
int rmInsertRecord(const std::string &fileName, const std::vector<element> &entry, const table &datatable);
void rmDeleteWithIndex(const std::string fileName,int offset, const Fitter &fitter, const table &datatable);
void rmDeleteWithoutIndex(const std::string fileName,const Fitter &fitter, const table &datatable);
std::vector <std::vector <element> > rmSelectWithIndex(const std::string fileName, int offset, const Fitter &fitter, const table &datatable);
std::vector <std::vector <element> > rmSelectWithoutIndex(const std::string fileName, const Fitter &fitter, const table &datatable);
void rmAddIndex(const std::string dbName,const std::string BTreeName, const table &datatable, int itemIndex);
std::set<int> rmGetAllOffsets(const std::string &fileName);
void rmClear(const std::string fileName) ;
#endif
