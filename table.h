#ifndef _TABLE_H_
#define _TABLE_H_
#include <set>
#include <string>
#include <vector>
struct item {
	std::string name;
	int type;
	int length;
	bool unique;
    std::set<std::string> indices;
	bool operator ==(const item &rhs) const;
};
struct table {
	int entrySize;
	std::string name;
	std::vector <item> items;
	table (const std::string & name,const std::vector<item> &items);
	table ();	
	void write();
};

#endif
